package com.message.user.signup;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.message.user.signup.service.SignupMessagePublisher;

@SpringBootTest
class SignupApplicationTests {	
	
	@MockBean
	private SignupMessagePublisher publisher;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Test
	void contextLoads() throws Exception {
		try {
			rabbitTemplate.convertAndSend(publisher);
		}catch(Exception e) {
			
		}
		
	}
	
	
	
}
