package com.message.user.signup.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.message.user.signup.model.Signup;


public class SignupValidationUtil {

	private static final Logger logger = LoggerFactory.getLogger(SignupValidationUtil.class);
	
	public static String removeMiddleName(String fullName){
		if(fullName!=null && fullName.length()>0) {
			String[] spaceSeparatedWords = fullName.split(" ");
			String firstName = spaceSeparatedWords[0];
			String lastName = spaceSeparatedWords[spaceSeparatedWords.length - 1]; 
			return firstName+" "+lastName;
		}
		return fullName;
		
	}
	
	// Function to check String for only Alphabets 
    public static boolean isStringOnlyAlphabet(String str) 
    { 
        return ((str != null) 
                && (!str.equals("")) 
                && (str.matches("^[a-zA-Z]*$"))); 
    } 
	
	public static boolean empty( final String s ) {
		  // Null-safe, short-circuit evaluation.
		  return s == null || s.trim().isEmpty();
	}
	
	public static boolean validateFullName(String name) {
		if(empty(name) && !name.trim().contains(" ")) {	
			logger.info("FullName is either empty or does not have last name");
			return false;
		}
		return true;
	}	
	
	
	public Signup getValidUser(String fullName, String phone) {
		Signup su = new Signup();
		
		fullName = getValidFullName(fullName);
		phone =  FrenchPhoneValidationUtil.getSpacesRemovedAndAppendPrefix(su.getPhone());
		
		su.setFullName(fullName);
		su.setPhone(phone);
		
		return su;
	}
	
	public String getValidFullName(String fullName) {
		return removeMiddleName(fullName.trim());
	}

	
	
	
}
