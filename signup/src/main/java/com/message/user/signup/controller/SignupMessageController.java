package com.message.user.signup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.message.user.signup.model.Signup;
import com.message.user.signup.service.SignupMessagePublisher;


@RestController
public class SignupMessageController{	
	
	@Autowired
	SignupMessagePublisher signupMessagePublisher;	
	
		
	@RequestMapping(value = "producer")
	@ResponseBody
	public String producer(@RequestParam("fullName") String fullName,@RequestParam("phone") String phone) {				
		 signupMessagePublisher.publishUser(fullName, phone);
		return "200 : Successfully";
	}

	@RequestMapping("publish")	
	@ResponseBody
	public String userSignup(@RequestBody Signup signupUser) {		
		signupMessagePublisher.publishUser(signupUser);		
		return "Message sent to the RabbitMQ Successfully";
	}

	

}
