package com.message.user.signup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SignupApplication { 
	
	private static final Logger logger = LoggerFactory.getLogger(SignupApplication.class);

	public static void main(String[] args) {
		logger.info("Welcome to RabbitMQ.....");
		SpringApplication.run(SignupApplication.class, args);
	}

}
