package com.message.user.signup.util;

public class FrenchPhoneValidationUtil {

	public static boolean validatePhoneNumber(String str) {
		if (str != null && str.length() > 0) {
			str = str.replaceAll("\\s", "");
			if (!str.startsWith("+33") ) {				
				return havingOnlyDigits(str);
			} else {
				str = str.substring(1);
				return havingOnlyDigits(str);	
			}
		}
		return true;

	}
	public static boolean havingOnlyDigits(String str) {
		return ((str != null) && (!str.equals("")) && (str.matches("^[0-9]*$")));
	}
	public static String getSpacesRemovedAndAppendPrefix(String phn) {
		if(phn!=null ) {
			if(!phn.startsWith("+33")) {
			phn= "+33"+phn;
			}
			phn =phn.replaceAll("\\s", "");
		}
		
		return phn;
	}

}
