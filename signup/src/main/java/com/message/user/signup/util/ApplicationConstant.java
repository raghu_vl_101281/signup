package com.message.user.signup.util;

import org.springframework.beans.factory.annotation.Value;

public final class ApplicationConstant {
	
	public static final String succerss_message = "200 : Success";
	public static final String failure_message = "400 : Error";
	
	
	@Value("${signup.response.success}")
	private static String success_msg;
	
	
	@Value("${signup.response.error}")
	private static String error_msg;
	

}
