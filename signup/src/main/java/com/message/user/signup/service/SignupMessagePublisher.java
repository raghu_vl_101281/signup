package com.message.user.signup.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.message.user.signup.model.Signup;
import com.message.user.signup.util.FrenchPhoneValidationUtil;
import com.message.user.signup.util.SignupValidationUtil;

@Service
public class SignupMessagePublisher {
	private static final Logger logger = LoggerFactory.getLogger(SignupMessagePublisher.class);
		
	@Autowired
	private AmqpTemplate amqpTemplate;
	
	@Value("${signup.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${signup.rabbitmq.routingkey}")
	private String routingkey;	
	
	
	
	public void send(String fullName, String phone) {
		
		Signup signupUser = new Signup(fullName,phone);			
		amqpTemplate.convertAndSend(exchange, routingkey, signupUser);
		logger.info("Sending msg to Queue ...  " + signupUser);
	    
	}

	
	public void publishUser( Signup su) {	
		logger.info(" At Publisher : fullName : "+ su.getFullName() +" - phone : "+ su.getPhone());
		su.setFullName(SignupValidationUtil.removeMiddleName(su.getFullName().trim()));
		su.setPhone(FrenchPhoneValidationUtil.getSpacesRemovedAndAppendPrefix(su.getPhone()));
		
		if(su!=null && su.getFullName()!=null && su.getFullName().length() >0) {
			send( su.getFullName(), su.getPhone());	
//			return ApplicationConstant.succerss_message;
			
			}
		else {
			logger.error("Invalid User Name");
//			return ApplicationConstant.failure_message
		}
				
		
	}

	public void publishUser(String fullName, String phone) {
		logger.info(" At Publisher : fullName : "+ fullName +" - phone : "+ phone);
		fullName = (SignupValidationUtil.removeMiddleName(fullName.trim()));
		phone = (FrenchPhoneValidationUtil.getSpacesRemovedAndAppendPrefix(phone));
		
		if(fullName!=null && fullName.length()>0) {
			send(fullName,phone);	
//			return ApplicationConstant.succerss_message;
		}
		else {
			logger.error("Invalid User Name");
//			return ApplicationConstant.failure_message
		}
			
	}
	
	

}
