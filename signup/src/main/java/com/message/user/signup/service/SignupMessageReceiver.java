package com.message.user.signup.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.message.user.signup.model.Signup;

@Component
public class SignupMessageReceiver {
	
	private static final Logger logger = LoggerFactory.getLogger(SignupMessageReceiver.class);
	
	@RabbitListener(queues = "${signup.rabbitmq.queue}")
	public void recievedMessage(Signup signupUser) {
		logger.info("###### Recieved Message From RabbitMQ: " + signupUser);
	}

}
