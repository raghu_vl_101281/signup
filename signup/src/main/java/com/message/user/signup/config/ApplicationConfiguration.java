package com.message.user.signup.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ApplicationConfiguration {
		
		private static final Logger logger = LoggerFactory.getLogger(ApplicationConfiguration.class);

		@Value("${signup.rabbitmq.queue}")
		private String queueName;

		@Value("${signup.rabbitmq.exchange}")
		private String exchange;

		@Value("${signup.rabbitmq.routingkey}")
		private String routingkey;

		@Bean
		Queue queue() {
			return new Queue(queueName, false);
		}

		@Bean
		DirectExchange exchange() {
			return new DirectExchange(exchange);
		}

		@Bean
		Binding binding(Queue queue, DirectExchange exchange) {
			logger.info("Binding queue to exchange with routingkay.....");
			return BindingBuilder.bind(queue).to(exchange).with(routingkey);
		}

		@Bean
		public MessageConverter jsonMessageConverter() {
			logger.info("JSON Message Converter.....");
			return new Jackson2JsonMessageConverter();
		}

		public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
			logger.info("AmqpTemplate ..... ");
			final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
			rabbitTemplate.setMessageConverter(jsonMessageConverter());
			return rabbitTemplate;
		}

}
